import { connect } from 'react-redux';
import { useEffect } from 'react';
import { getPosts } from './redux/actions/postsActions';
import './App.scss';

function App(props) {
    useEffect(() => {
        props.dispatch(getPosts());
    }, []);
    const showPosts = () => {
        if (props.loading) return <div>Loading...</div>;
        if (props.error) return <div>Error</div>;
        return props.posts.map((post) => <p key={post.id}>{post.title}</p>);
    };
    return (
        <div className="app">
            <h1>Working</h1>
            {showPosts()}
        </div>
    );
}

const mapStateToProps = ({ posts: { loading, posts, error } }) => ({
    loading: loading,
    posts: posts,
    error: error,
});

export default connect(mapStateToProps)(App);
