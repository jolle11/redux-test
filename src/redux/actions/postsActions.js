export const GET_POSTS = 'GET_POSTS';
export const GET_POSTS_OK = 'GET_POSTS_OK';
export const GET_POSTS_ERROR = 'GET_POSTS_ERROR';

const actionGetPosts = () => ({
    type: GET_POSTS,
});

const actionGetPostsOk = (posts) => ({
    type: GET_POSTS_OK,
    payload: posts,
});

const actionGetPostsError = (posts) => ({
    type: GET_POSTS_ERROR,
});

export const getPosts = () => {
    return async (dispatch) => {
        // 1. Advice that it's going to get the posts. Does fetch
        // 2. When fetch comes back, OK or ERROR
        dispatch(actionGetPosts());
        try {
            const response = await fetch('https://jsonplaceholder.typicode.com/posts');
            const data = await response.json();

            dispatch(actionGetPostsOk(data));
        } catch (error) {
            console.log(error);
            dispatch(actionGetPostsError());
        }
    };
};
